/**
 * New js file
 */

var socket;

var wshelper = {

	joinWorld : function(worldname) {
		socket.send(JSON.stringify({
			world : worldname,
			type: TYPE_JOIN_WORLD
		}));
	},
	leaveWorld : function() {
		socket.send(JSON.stringify({
			type: TYPE_LEAVE_WORLD
		}));
	},
	sendPositions : function(currentworld, positionarray) {
		socket.send(JSON.stringify({
			world : currentworld,
			worldstate : positionarray,
			type : 'WORLD'
		}));
	},

	sendDelta : function(currentworld, deltaarray) {
		socket.send(JSON.stringify({
			type : TYPE_WORLDDELTA,
			world : currentworld,
			deltas : deltaarray
		}));
	},

	acquireLock : function(currentworld, piece) {
		socket.send(JSON.stringify({
			type : TYPE_GETLOCK,
			world : currentworld,
			piece : piece
		}));
	},
	releaseLock : function(currentworld, piece) {
		socket.send(JSON.stringify({
			type : TYPE_RELEASELOCK,
			world : currentworld,
			piece : piece
		}));
	},
	sendText : function(message) {
		socket.send(JSON.stringify({
			text : message,
			type : 'TEXT'
		}));
	},

	setupSocket : function() {
		socket = new WebSocket("ws://localhost:" + port);
		socket.onopen = function(event) {
			console.log("connected");
		};
		socket.onclose = function(event) {
			// alert("closed code:" + event.code + " reason:" + event.reason
			// + " wasClean:" + event.wasClean);
		};

		socket.onmessage = function(event) {

			console.log(event.data);
			var received_obj = JSON.parse(event.data);

			if (received_obj.type == TYPE_WORLDUPDATE) {
				if ($(this).attr('id') != CURRENT_DRAGGED_ITEM) {
					$('#world')
							.children()
							.each(
									function() {
										$(this)
												.animate(
														{
															'left' : received_obj[worldstate][CURRENT_WORLD][$(
																	this).attr(
																	'id')]['x'],
															'top' : received_obj[worldstate][CURRENT_WORLD][$(
																	this).attr(
																	'id')]['y'],
														}, 500, null);

									});

				}
			}
		};
	},

}
