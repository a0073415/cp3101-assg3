if (typeof window.DeviceMotionEvent != 'undefined') {

	// set the threshhold to detect shaking
	var threshold = 15;

	// Initialize orientation variables
	/* var x1 = 0, y1 = 0, z1 = 0, x2 = 0, y2 = 0, z2 = 0; */

	var x1 = x2 = y1 = y2 = z1 = z2 = 0;

	var lastTime = new Date();
	var currentTime;

	var firstShake = false;

	// Listen to motion events and update the position
	window.addEventListener('devicemotion', function(e) {
		x1 = e.accelerationIncludingGravity.x;
		y1 = e.accelerationIncludingGravity.y;
		z1 = e.accelerationIncludingGravity.z;
	}, false);

	// Check the position of phone every 1/4 seconds. If there is a change in
	// position, check whether it exceeds threshhold. if exceeds threshhold, it
	// is a shake, fire up the event.
	setInterval(function() {
		var orientation_diff = Math.abs(x1 - x2 + y1 - y2 + z1 - z2);

		if (orientation_diff > threshold && !(firstShake) && CURRENT_WORLD != '') {

			var response = confirm ("Reset the game?");
			
			if(response){
				wshelper.resetWorld(CURRENT_WORLD);
			}
			else{
				//do nothing
			}

			// set firstShake flag = true. Reset it after 3 seconds
			// This is to prevent the event being fired twice, cos there will be
			// two orientation_diff happening: still-> shake and shake -> still.
			firstShake = true;
			setTimeout(function() {
				firstShake = false;
			}, 3000);
		}

		// Update new position
		x2 = x1;
		y2 = y1;
		z2 = z1;

	},
	// the interval for detecting change.
	250);

}