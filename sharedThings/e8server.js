// Remember to require config.js for the port number
var config = require('./config_node.js');
var constants = require('./constants.js');
var helper = require('./helper.js');

var WebSocketServer = require('ws').Server, wss = new WebSocketServer({
	port : config.port
});

wss.on('close', function() {
	console.log('disconnected');
});

wss.broadcast = function(data) {
	for (var i in this.clients) {
		this.clients[i].send(data);
	}
};

// World specific broadcast method
wss.broadcastToWorld = function(worldToBroadcast, data) {
	for (var i in this.clients) {
		if (this.clients[i].world == worldToBroadcast) {
			this.clients[i].send(data);
		}
	}
};

// Get global state of players in each world
// Used for main landing page
wss.getGlobalWorld = function() {
	var returnmsg = {};
	returnmsg[constants.type] = constants.TYPE_GLOBAL_WORLDSTATE;
	var global_worldstate = helper.getGlobalWorldState();
	var worlds_with_players = {};
	for (var key in global_worldstate) {
		worlds_with_players[key] = {};
		worlds_with_players[key].users = [];
		worlds_with_players[key].world_id = key;
		for (var clientkey in wss.clients) {
			if (wss.clients[clientkey].world === key) {
				if (wss.clients[clientkey].user === '') {
					worlds_with_players[key].users.push('Anonymous');
				} else {
					worlds_with_players[key].users.push(wss.clients[clientkey].user);
				}
			}

		}
	}
	returnmsg[constants.worldstate] = worlds_with_players;
	return returnmsg;
};

wss.on('connection', function(ws) {
	// console.log(ws);
	ws.world = '';
	ws.user = '';
	ws.lock = '';

	// get player state in this world, used for display table within world
	ws.getPlayerUpdate = function(world) {

		var players_obj = {};
		for (var key in wss.clients) {
			var socket_fd = wss.clients[key]._socket._handle.fd;

			//not self
			if (wss.clients[key].world === world) {
				players_obj[wss.clients[key]._socket._handle.fd] = {
					user : wss.clients[key].user,
					locked_piece : wss.clients[key].lock
				};
			}

		}
		var player_return_msg = {};
		player_return_msg[constants.type] = constants.TYPE_GET_PLAYERS;
		player_return_msg[constants.players] = players_obj;
		return player_return_msg;
	};

	ws.getLockedStatesMsg = function() {
		var lock_states_bcast = {};
		lock_states_bcast[constants.type] = constants.TYPE_LOCKED_PIECES;
		lock_states_bcast[constants.lockstates] = helper.getLockState(ws.world);
		return lock_states_bcast;
	};

	ws.on('message', function(message) {
		var fd = ws._socket._handle.fd;
		console.log("Received message from %s", fd);

		try {
			var parsed_message = JSON.parse(message);
			console.log(parsed_message.type);

			switch (parsed_message.type) {
				case constants.TYPE_GET_GLOBAL_WORLDSTATE:
					ws.send(JSON.stringify(wss.getGlobalWorld()));
					break;
				case constants.TYPE_JOIN_WORLD:
					ws.world = parsed_message.world;
					helper.createWorld(parsed_message.world);

					// Send initial broadcast of worldstate
					var updatebcast = {};
					updatebcast[constants.type] = constants.TYPE_WORLDUPDATE;
					updatebcast[constants.worldstate] = helper.getWorldState(ws.world);
					ws.send(JSON.stringify(updatebcast));
					var fdsend = {};
					fdsend[constants.type] = constants.TYPE_FD;
					fdsend[constants.myfd] = fd;
					ws.send(JSON.stringify(fdsend));
					wss.broadcastToWorld(ws.world, JSON.stringify(ws.getPlayerUpdate(parsed_message.world)));
					wss.broadcast(JSON.stringify(wss.getGlobalWorld()));
					break;

				case constants.TYPE_LEAVE_WORLD:
					var oldworld = ws.world;
					ws.world = '';
					wss.broadcastToWorld(oldworld, JSON.stringify(ws.getPlayerUpdate(oldworld)));
					wss.broadcastToWorld(oldworld, JSON.stringify(wss.getGlobalWorld()));
					break;

				case constants.TYPE_DELETE_WORLD:
					helper.deleteWorld(parsed_message.world);
					for (var clientkey in wss.clients) {
						if (wss.clients[clientkey].world === parsed_message.world) {
							wss.clients[clientkey].world = '';
							wss.clients[clientkey].lock = '';
						}
					}
					wss.broadcast(JSON.stringify(wss.getGlobalWorld()));
					break;

				case constants.TYPE_RESET_WORLD:
					helper.resetWorld(parsed_message.world);
					var updatebcast = {};
					updatebcast[constants.type] = constants.TYPE_WORLDUPDATE;
					updatebcast[constants.worldstate] = helper.getWorldState(parsed_message.world);
					wss.broadcastToWorld(parsed_message.world, JSON.stringify(updatebcast));
					var lock_states_bcast = {};
					lock_states_bcast[constants.type] = constants.TYPE_LOCKED_PIECES;
					lock_states_bcast[constants.lockstates] = helper.getLockState(ws.world);
					wss.broadcastToWorld(ws.world, JSON.stringify(lock_states_bcast));
					break;

				case constants.TYPE_TEXT:
					console.log(parsed_message.text);
					break;
				case constants.TYPE_GETLOCK:
					var ac_lock_res = helper.acquireLock(parsed_message.world, fd, parsed_message.piece);
					console.log("%s acquire lock res: %s", fd, ac_lock_res);
					// consider returning success/error
					if (!ac_lock_res) {
						var returnerror = {};
						returnerror[constants.type] = constants.TYPE_ERROR;
						returnerror[constants.errormsg] = "Lock not successfully acquired";
						ws.send(JSON.stringify(returnerror));
					} else {
						ws.lock = parsed_message.piece;
					}

					wss.broadcastToWorld(ws.world, JSON.stringify(ws.getLockedStatesMsg()));
					wss.broadcastToWorld(ws.world, JSON.stringify(ws.getPlayerUpdate(ws.world)));
					break;
				case constants.TYPE_RELEASELOCK:
					var rel_lock_res = helper.releaseLock(parsed_message.world, fd, parsed_message.piece);
					console.log("%s release lock res: %s", fd, rel_lock_res);
					if (rel_lock_res) {
						ws.lock = '';
					}
					wss.broadcastToWorld(ws.world, JSON.stringify(ws.getLockedStatesMsg()));
					wss.broadcastToWorld(ws.world, JSON.stringify(ws.getPlayerUpdate(ws.world)));
					break;
				case constants.TYPE_WORLDDELTA:

					helper.updateWorld(parsed_message.world, fd, parsed_message.deltas);
					var updatebcast = {};
					updatebcast[constants.type] = constants.TYPE_WORLDUPDATE;
					updatebcast[constants.worldstate] = helper.getWorldState(ws.world);
					wss.broadcastToWorld(ws.world, JSON.stringify(updatebcast));
					break;

				case constants.TYPE_GET_LOCKED_PIECES:

					ws.send(JSON.stringify(ws.getLockedStatesMsg()));
					break;
				case constants.TYPE_GET_PLAYERS:

					ws.send(JSON.stringify(ws.getPlayerUpdate(parsed_message.world)));
					break;
				case constants.TYPE_SET_USER:
					ws.user = parsed_message.user;
					wss.broadcastToWorld(ws.world, JSON.stringify(ws.getPlayerUpdate(ws.world)));
					break;
				default:
					break;

			}
			// helper.saveWorld(message);
		} catch (e) {
			console.log(e);
		}

	});

	ws.on('close', function() {
		// Any unregistering of stuff goes here
	});
	console.log("Client connected");
});
