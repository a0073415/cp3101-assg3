/**
 * 
 */

var world = 'world';
var worldstate = 'worldstate';
var type = 'type';
var text = 'text';
var user = 'user';
var deltas = 'deltas';
var piece = 'piece';
var screensizes = 'screensizes';
var lockstates = 'lockstates';
var players = 'players';
var myfd = 'myfd';

var errormsg = 'error_message';

// Client to server types
var TYPE_TEXT = 'TEXT';
var TYPE_WORLD = 'WORLD';
var TYPE_SET_USER = 'SET_USER';
var TYPE_WORLDDELTA = 'WORLDDELTA';
var TYPE_GETLOCK = 'GETLOCK';
var TYPE_RELEASELOCK = 'RELEASELOCK';
var TYPE_GET_GLOBAL_WORLDSTATE = 'GLOBAL_WORLDSTATE';
var TYPE_JOIN_WORLD = 'JOIN_WORLD';
var TYPE_LEAVE_WORLD = 'LEAVE_WORLD';
var TYPE_DELETE_WORLD = 'DELETE_WORLD';
var TYPE_RESET_WORLD = 'RESET_WORLD';
var TYPE_SET_SCREENSIZE = 'SET_SCREENSIZE';
var TYPE_GET_SCREENSIZES = 'GET_SCREENSIZES';
var TYPE_GET_LOCKED_PIECES = 'GET_LOCKEDPIECES';
var TYPE_GET_PLAYERS = 'GET_PLAYERS';

// Server to client types
var TYPE_GLOBAL_WORLDSTATE = 'GLOBAL';
var TYPE_WORLDUPDATE = 'WORLDUPDATE';
var TYPE_CLIENT_SCREENSIZES = 'SCREENSIZES';
var TYPE_LOCKED_PIECES = 'LOCKED_PIECES';
var TYPE_ERROR = 'ERROR';
var TYPE_FD = 'FD';

try {
	module.exports.world = world;
	module.exports.worldstate = worldstate;
	module.exports.type = type;
	module.exports.text = text;
	module.exports.user = user;
	module.exports.deltas = deltas;
	module.exports.piece = piece;
	module.exports.errormsg = errormsg;
	module.exports.screensizes = screensizes;
	module.exports.lockstates = lockstates;
	module.exports.players = players;
	module.exports.myfd = myfd;
	module.exports.TYPE_TEXT = TYPE_TEXT;
	module.exports.TYPE_WORLD = TYPE_WORLD;
	module.exports.TYPE_SET_USER = TYPE_SET_USER;
	module.exports.TYPE_WORLDDELTA = TYPE_WORLDDELTA;
	module.exports.TYPE_GETLOCK = TYPE_GETLOCK;
	module.exports.TYPE_RELEASELOCK = TYPE_RELEASELOCK;
	module.exports.TYPE_GET_GLOBAL_WORLDSTATE = TYPE_GET_GLOBAL_WORLDSTATE;
	module.exports.TYPE_JOIN_WORLD = TYPE_JOIN_WORLD;
	module.exports.TYPE_LEAVE_WORLD = TYPE_LEAVE_WORLD;
	module.exports.TYPE_RESET_WORLD = TYPE_RESET_WORLD;
	module.exports.TYPE_DELETE_WORLD = TYPE_DELETE_WORLD;
	module.exports.TYPE_SET_SCREENSIZE = TYPE_SET_SCREENSIZE;
	module.exports.TYPE_GET_SCREENSIZES = TYPE_GET_SCREENSIZES;
	module.exports.TYPE_GET_LOCKED_PIECES = TYPE_GET_LOCKED_PIECES;
	module.exports.TYPE_GLOBAL_WORLDSTATE = TYPE_GLOBAL_WORLDSTATE;
	module.exports.TYPE_WORLDUPDATE = TYPE_WORLDUPDATE;
	module.exports.TYPE_CLIENT_SCREENSIZES = TYPE_CLIENT_SCREENSIZES;
	module.exports.TYPE_LOCKED_PIECES = TYPE_LOCKED_PIECES;
	module.exports.TYPE_GET_PLAYERS = TYPE_GET_PLAYERS;
	module.exports.TYPE_FD = TYPE_FD;
	module.exports.TYPE_ERROR = TYPE_ERROR;
} catch (e) {
	// Probably client side error with module not found.
}