# Extra Things Done

1) Indicators to show locked pieces
2) List of players with the pieces they are holding on to
3) Mobile optimized with shake feature, can be played on most mobile devices. (Tested on mobile firefox on 
Firefox (Android), Chrome(Android /iOS), Safari (iOS).
4) Error messages from the backend will be displayed.
5) Shaking mobile device (with accelerometer) will reset the game.