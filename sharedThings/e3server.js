// Remember to require config.js for the port number
var config = require('./config_node.js');
var constants = require('./constants.js');

var WebSocketServer = require('ws').Server, wss = new WebSocketServer({
	port : config.port
});

var GLOBAL_WORLDSTATE = [];

wss.on('close', function() {
	console.log('disconnected');
});

wss.broadcast = function(data) {
	for ( var i in this.clients)
		this.clients[i].send(data);
};

wss.on('connection', function(ws) {

	ws.on('message', function(message) {
		try {

			var world_obj = JSON.parse(message);
			console.log(world_obj);
			var world_to_update = world_obj[constants.worldname];
			var world_state = world_obj[constants.worldstate];
			GLOBAL_WORLDSTATE[world_to_update] = world_state;

			console.log("Saved %s to world %s", message, world_to_update);

		} catch (e) {
			console.log("Unparsable JSON: %s", message);
		}

	});
	console.log("Client connected");
});