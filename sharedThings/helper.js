/**
 * Server side functions
 */
var constants = require('./constants.js');

var GLOBAL_WORLDSTATE = {};
var GLOBAL_LOCKS = {};

var default_world_state = {
	"x1" : {
		"x" : 8,
		"y" : 8
	},
	"x2" : {
		"x" : 8,
		"y" : 8
	},
	"x3" : {
		"x" : 8,
		"y" : 8
	},
	"x4" : {
		"x" : 8,
		"y" : 8
	},
	"x5" : {
		"x" : 8,
		"y" : 8
	},
	"o1" : {
		"x" : 8,
		"y" : 8
	},
	"o2" : {
		"x" : 8,
		"y" : 8
	},
	"o3" : {
		"x" : 8,
		"y" : 8
	},
	"o4" : {
		"x" : 8,
		"y" : 8
	},
	"o5" : {
		"x" : 8,
		"y" : 8
	},
	"tttboard" : {
		"x" : 8,
		"y" : 8
	}
};

var default_locks = {
	"x1" : -1,
	"x2" : -1,
	"x3" : -1,
	"x4" : -1,
	"x5" : -1,
	"o1" : -1,
	"o2" : -1,
	"o3" : -1,
	"o4" : -1,
	"o5" : -1,
	"tttboard" : -1
};

function isEmptyObject(obj) {
	return !Object.keys(obj).length;
}

module.exports = {
	// Do not edit first two functions, as they are used in e1 to e5
	saveWorld : function(world_obj) {

		var world_to_update = world_obj.world;
		var world_state = world_obj.worldstate;
		GLOBAL_WORLDSTATE[world_to_update] = world_state;
		console.log("Saved to world %s", world_to_update);
	},

	getGlobalWorldState : function() {
		console.log(GLOBAL_WORLDSTATE);
		return GLOBAL_WORLDSTATE;
	},

	// e7 onwards:
	getWorldState : function(worldname) {
		return GLOBAL_WORLDSTATE[worldname];
	},
	updateWorld : function(worldname, fd, deltas) {

		// update deltas
		for ( var keys in deltas) {
			// e.g keys = o1, untested
			if (GLOBAL_LOCKS[worldname][keys] === fd) {
				// must have lock to edit
				console.log(deltas);
				GLOBAL_WORLDSTATE[worldname][keys].x += parseInt(
						deltas[keys].x, 10);
				GLOBAL_WORLDSTATE[worldname][keys].y += parseInt(
						deltas[keys].y, 10);
			}
		}

	},

	createWorld : function(worldname) {
		if (!(worldname in GLOBAL_WORLDSTATE)) {
			// initialize if new world
			GLOBAL_WORLDSTATE[worldname] = JSON.parse(JSON.stringify(default_world_state));
		}
		if (!(worldname in GLOBAL_LOCKS)) {
			GLOBAL_LOCKS[worldname] = JSON.parse(JSON.stringify(default_locks));
		}
	},
	deleteWorld : function(worldname) {
		delete GLOBAL_LOCKS[worldname];
		delete GLOBAL_WORLDSTATE[worldname];
	},
	resetWorld : function(worldname) {
		GLOBAL_LOCKS[worldname] = JSON.parse(JSON.stringify(default_locks));
		GLOBAL_WORLDSTATE[worldname] = JSON.parse(JSON.stringify(default_world_state));
	},

	// Locks
	acquireLock : function(worldname, fd, piece) {
		if (!(worldname in GLOBAL_LOCKS)) {
			GLOBAL_LOCKS[worldname] = JSON.parse(JSON.stringify(default_locks));
		}

		// simple test and set
		// as javascript is single threaded, we assume entire function
		// is atomic
		if (GLOBAL_LOCKS[worldname][piece] === -1) {
			GLOBAL_LOCKS[worldname][piece] = fd;
			return true;
		} else {
			return false;
		}
	},
	releaseLock : function(worldname, fd, piece) {
		if (!(worldname in GLOBAL_LOCKS)) {
			return false;
		}
		if (GLOBAL_LOCKS[worldname][piece] === fd) {
			GLOBAL_LOCKS[worldname][piece] = -1;
			return true;
		} else {
			return false;
		}
	},
	getLockState : function(worldname) {
		return GLOBAL_LOCKS[worldname];
	},

};
