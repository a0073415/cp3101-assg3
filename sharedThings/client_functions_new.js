/**
 * New functions for e7 onwards
 */

var socket;
var spill_player_table;
function getRandomColor() {
	var letters = '0123456789ABCDEF'.split('');
	var color = '#';
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}

var wshelper = {

	getWorlds : function() {
		socket.send(JSON.stringify({
			type : TYPE_GET_GLOBAL_WORLDSTATE
		}));
	},
	joinWorld : function(worldname) {
		socket.send(JSON.stringify({
			type : TYPE_JOIN_WORLD,
			world : worldname
		}));
	},
	leaveWorld : function() {
		socket.send(JSON.stringify({
			type : TYPE_LEAVE_WORLD
		}));
		CURRENT_WORLD = "";
	},
	resetWorld : function(worldname) {
		socket.send(JSON.stringify({
			type : TYPE_RESET_WORLD,
			world : worldname
		}));
	},
	deleteWorld : function(worldname) {
		socket.send(JSON.stringify({
			type : TYPE_DELETE_WORLD,
			world : worldname
		}));
	},
	sendPositions : function(currentworld, positionarray) {
		socket.send(JSON.stringify({
			world : currentworld,
			worldstate : positionarray,
			type : 'WORLD'
		}));
	},

	sendDelta : function(currentworld, deltaarray) {
		socket.send(JSON.stringify({
			type : TYPE_WORLDDELTA,
			world : currentworld,
			deltas : deltaarray
		}));
	},

	acquireLock : function(currentworld, piece) {
		socket.send(JSON.stringify({
			type : TYPE_GETLOCK,
			world : currentworld,
			piece : piece
		}));
	},
	releaseLock : function(currentworld, piece) {
		socket.send(JSON.stringify({
			type : TYPE_RELEASELOCK,
			world : currentworld,
			piece : piece
		}));
	},
	getLocks : function(currentworld) {
		socket.send(JSON.stringify({
			type : TYPE_GET_LOCKED_PIECES,
			world : currentworld,
		}));
	},
	getPlayers : function(currentworld) {
		socket.send(JSON.stringify({
			type : TYPE_GET_PLAYERS,
			world : currentworld,
		}));
	},
	sendText : function(message) {
		socket.send(JSON.stringify({
			text : message,
			type : 'TEXT'
		}));
	},

	setUser : function(user) {
		socket.send(JSON.stringify({
			type : TYPE_SET_USER,
			user : user
		}));
	},
	setupSocket : function(onconnected) {
		console.log(document.location.hostname);
		socket = new WebSocket("ws://" + document.location.hostname + ":" + port);
		socket.onopen = function(event) {
			console.log("connected");
			if (onconnected !== undefined) {
				onconnected();
			}
		};
		socket.onclose = function(event) {
			// alert("closed code:" + event.code + " reason:" + event.reason
			// + " wasClean:" + event.wasClean);
		};

		socket.onmessage = function(event) {

			console.log(event.data);
			var received_obj = JSON.parse(event.data);

			switch (received_obj.type) {
				case TYPE_WORLDUPDATE:

					console.log(CURRENT_DRAGGED_ITEM);
					$('#world').children().each(function() {
						if ($(this).attr('id') != CURRENT_DRAGGED_ITEM) {
							$(this).animate({
								'left' : received_obj[worldstate][$(this).attr('id')].x,
								'top' : received_obj[worldstate][$(this).attr('id')].y
							}, 500, null);
						}

					});

					break;
				case TYPE_GLOBAL_WORLDSTATE:
					//FH THIS IS WHERE THE WORLDS ARE RETURNED

					console.log(received_obj.worldstate['qwe']);
					console.log("Received ws");
					//					$(".server-sent-worldid").append(received_obj.worldstate['123'].world_id);
					$(".list-of-worlds-spill").empty();
					for (var worlds in received_obj.worldstate) {
						var alias_world_id = received_obj.worldstate[worlds].world_id;
						$(".list-of-worlds-spill").append('<div class="col-xs-12 col-md-2"><div class="thumbnail"><img src="images/world-bg.png" alt="Generic placeholder thumbnail"></div><div class="caption"><h3>World: ' + alias_world_id + '</h3><p><a href="#" onclick="return joinWorldFunc(this);" class="btn btn-primary join-world-button" role="button" data-worldname="' + alias_world_id + '"> Join </a> <a href="#" class="btn btn-danger delete-world-button" onclick="return delWorldFunc(this);" role="button" data-worldname="' + alias_world_id + '">Delete </a></p></div></div>');
					}
					break;
				case TYPE_LOCKED_PIECES:
					//					console.log(received_obj.lockstates);
					$('#world').children().each(function() {

						if ($(this).attr('id') !== CURRENT_DRAGGED_ITEM) {
							if (received_obj.lockstates[$(this).attr('id')] !== -1) {
								$(this).find('#lock-text').html(PLAYERS_LIST[received_obj.lockstates[$(this).attr('id')]].user);
								$(this).css('background-color', '#E3B0F7');
								$(this).data('locked', true);
							} else {
								$(this).find('#lock-text').html('');
								$(this).css('background-color', '');
								$(this).data('locked', false);
							}
						}
					});

					break;
				case TYPE_GET_PLAYERS:
					//					console.log(received_obj.players);
					PLAYERS_LIST = received_obj.players;
					var table_data = '';
					$.each(PLAYERS_LIST, function(i, val) {
						var user = val.user;
						if (user == ''){
							user = 'Anonymous';
						}
						table_data += ("<tr><td>" + user + "</td><td>" + val.locked_piece + "</td></tr>");
					});
					spill_player_table = "<table><tr><th>Players</th><th>Moving Piece</th></tr>" + table_data + "</table>";
					console.log(spill_player_table);
					$(".panel-player-list").html(spill_player_table);
					break;
				case TYPE_FD:
					CURRENT_FD = received_obj.myfd;
				case TYPE_ERROR:
					showError(received_obj.errormsg);
					break;
				default:
					break;
			}

		};
	},
};
