// Remember to require config.js for the port number
var config = require('./config_node.js');
var constants = require('./constants.js');
var helper = require('./helper.js');

var WebSocketServer = require('ws').Server, wss = new WebSocketServer({
	port : config.port
});

wss.on('close', function() {
	console.log('disconnected');
});

wss.broadcast = function(data) {
	for ( var i in this.clients)
		this.clients[i].send(data);
};

wss.on('connection', function(ws) {

	ws.on('message', function(message) {

		try {
			var parsed_message = JSON.parse(message);
			console.log(parsed_message.type);
			switch (parsed_message.type) {
			case constants.TYPE_TEXT:
				console.log(parsed_message.text);
				break;
			case constants.TYPE_WORLD:
				console.log("received world");
				helper.saveWorld(parsed_message);


				var updatebcast = {};
				updatebcast[constants.type] = constants.TYPE_WORLDUPDATE;
				updatebcast[constants.worldstate] = helper
						.getGlobalWorldState();
				wss.broadcast(JSON.stringify(updatebcast));

				break;
			default:
				break;

			}
			// helper.saveWorld(message);
		} catch (e) {
			console.log(e);
		}

	});
	console.log("Client connected");
});
